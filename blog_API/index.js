const express = require('express')
const app = express()
require('dotenv/config')
const port = process.env.PORT
const multer = require('multer')

require('./db')

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, "images");
	},
	filename: (req, file, cb) => {
		let ext = file.mimetype.split('/')[1]
		cb(null, `image-${Date.now()}.${ext}`);
	},
});


const upload = multer({ storage: storage })

app.post("/api/upload", upload.single("file"), (req, res) => {
    res.status(200).send({message: "Image uplaoded."})
})


app.use(express.json())


app.use('/api/users', require('./routes/users'))								
app.use('/api/posts', require('./routes/posts'))								
app.use('/api/categories', require('./routes/categories'))															

app.listen(port , () => console.log(`Running on port ${port}`))

