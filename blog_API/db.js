const mongoose = require('mongoose')

module.export = mongoose.connect(
    process.env.DB_URI, {
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false

})
.then(()=>console.log("Connected to db"))
.catch(err=>console.log(err))
