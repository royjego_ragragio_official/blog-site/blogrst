const User = require('./../models/User')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const {createAccessToken} = require("../utils/auth")
require('dotenv/config')
const {sendRegisterMail,sendRegistryMail,sendChangePasswordMail} = require('../utils/sendMail')

module.exports.register = async (req,res) => {
	const hashedPw = await bcrypt.hashSync(req.body.password, 10)
	if(req.body.password.length < 8) return res.status(400).send({error: "Password is too short."})
	if (req.body.password !== req.body.confirmPassword) return res.status(400).send({error:"Passwords don't match."})

	let newUser = new User({
			username: req.body.username.toLowerCase(),
			firstName: req.body.firstName.toLowerCase(),
			lastName: req.body.lastName.toLowerCase(),
			email: req.body.email.toLowerCase(),
			password: hashedPw
	})
	await newUser.save()
	.then(user=>{
		sendRegistryMail(req.body)
		sendRegisterMail(req.body)
		res.status(200).send({user,mesage: "Registration Successful!"})
	})
	.catch(error=>{
		res.status(500).send(error)
	})
}

module.exports.login = async (req,res) => {
	await User.findOne({$or:[{email: req.body.user.toLowerCase()},{username: req.body.user.toLowerCase()}]})
	.then(foundUser => {
		if(foundUser === null){
			res.status(400).send({error: "Invalid credentials."})
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			if(isPasswordCorrect){
				res.status(200).send({accessToken: createAccessToken(foundUser), message: "Login Successful!"})
			} else {
				res.status(400).send({error: "Invalid credentials."})
			}
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getDetails = async (req,res) => {
	await User.findById(req.user.id, {_id:0,isAdmin:0,password: 0,createdAt:0,updatedAt:0,__v:0})
	.then(foundUser => {
		if(foundUser){
			res.status(200).send(foundUser)
		} else {
			res.status(404).send({message: "No user found."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.updateDetails = async (req,res) => {
	const user = await User.findById(req.user.id)
	let updates = {
		profilePic: req.body.profilePic===undefined?user.profilePic:req.body.profilePic,
		mobileNo: req.body.mobileNo===undefined?user.mobileNo:req.body.mobileNo,
		currentAddress: {
			line1: req.body.currentAddress.line1===undefined?user.currentAddress.line1:req.body.currentAddress.line1.toLowerCase(),
	        street: req.body.currentAddress.street===undefined?user.currentAddress.street:req.body.currentAddress.street.toLowerCase(),
			subdivision: req.body.currentAddress.subdivision===undefined?user.currentAddress.subdivision:req.body.currentAddress.subdivision.toLowerCase(),
	        district: req.body.currentAddress.district===undefined?user.currentAddress.district:req.body.currentAddress.district.toLowerCase(),
	        city: req.body.currentAddress.city===undefined?user.currentAddress.city:req.body.currentAddress.city.toLowerCase(),
	        province: req.body.currentAddress.province===undefined?user.currentAddress.province:req.body.currentAddress.province.toLowerCase(),
	        region: req.body.currentAddress.region===undefined?user.currentAddress.region:req.body.currentAddress.region.toLowerCase(),
	        country: req.body.currentAddress.country===undefined?user.currentAddress.country:req.body.currentAddress.country.toLowerCase(),
	        postalCode: req.body.currentAddress.postalCode===undefined?user.currentAddress.postalCode:req.body.currentAddress.postalCode
		},
		permanentAddress: {
			line1: req.body.permanentAddress.line1===undefined?user.permanentAddress.line1:req.body.permanentAddress.line1.toLowerCase(),
	        street: req.body.permanentAddress.street===undefined?user.permanentAddress.street:req.body.permanentAddress.street.toLowerCase(),
			subdivision: req.body.permanentAddress.subdivision===undefined?user.permanentAddress.subdivision:req.body.permanentAddress.subdivision.toLowerCase(),
	        district: req.body.permanentAddress.district===undefined?user.permanentAddress.district:req.body.permanentAddress.district.toLowerCase(),
	        city: req.body.permanentAddress.city===undefined?user.permanentAddress.city:req.body.permanentAddress.city.toLowerCase(),
	        province: req.body.permanentAddress.province===undefined?user.permanentAddress.province:req.body.permanentAddress.province.toLowerCase(),
	        region: req.body.permanentAddress.region===undefined?user.permanentAddress.region:req.body.permanentAddress.region.toLowerCase(),
	        country: req.body.permanentAddress.country===undefined?user.permanentAddress.country:req.body.permanentAddress.country.toLowerCase(),
	        postalCode: req.body.permanentAddress.postalCode===undefined?user.permanentAddress.postalCode:req.body.permanentAddress.postalCode
		}
	}
	await User.findByIdAndUpdate(req.user.id,updates,{new:true})
	.then( user => {
		res.status(200).send({user,message: `User has been updated.`})
	})
	.catch(error => res.status(500).send(error))
}


module.exports.changePassword = async (req,res) => {
	const user = await User.findById(req.user.id)
	const hashedPw = await bcrypt.hashSync(req.body.newPassword, 10)
	let updatedPassword = {
		password: hashedPw
	}
	await User.findByIdAndUpdate(req.user.id,updatedPassword,{new:true})
	.then( user => {
		sendChangePasswordMail(user)
		res.status(200).send({message: `Password has been updated.`})
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getActiveUsers = async (req,res) => {
	await User.find({isActive: true})
	.then(foundUsers=> {
		if(foundUsers.length>0){
			res.status(200).send(foundUsers)
		} else {
			res.status(404).send({message: "No active users found."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.deactivate = async (req,res) => {
	let user = await User.findById(req.user.id)
	if(user.isActive===false) {return res.status(400).send({message: "User is already inactive."})}
	await User.findByIdAndUpdate(req.user.id,{isActive:false},{new:true})
	.then(foundUser=>{
		console.log('this was still done')
		res.status(200).send({foundUser,mesage: "User has been deactivated."})
	})
	.catch(error => res.status(500).send(error))
}

module.exports.reactivate = async (req,res) => {
	let user = await User.findById(req.user.id)
	if(user.isActive===true) {return res.status(400).send({message: "User is already active."})}
	await User.findByIdAndUpdate(req.user.id,{isActive:true},{new:true})
	.then(foundUser=>{
		res.status(200).send({foundUser,mesage: "User has been reactivated."})
	})
	.catch(error => res.status(500).send(error))
}

module.exports.addFriend = async (req,res) =>{
	const friend = await User.findById(req.params.id)
	if(friend.isActive===false){return res.status(400).send({message: "Can not add an inactive user."})}
	User.findById(req.user.id)
	.then(foundUser=>{
		foundUser.friends.push({userId: friend._id, username: friend.username})
		return foundUser.save()
	})
	.then(updatedUser => {
		console.log(updatedUser.username, updatedUser.friends)
		return User.findById(friend._id)
	})
	.then(foundFriend => {
		foundFriend.friends.push({userId: req.user.id, username: req.user.username})
		return foundFriend.save()
	})
	.then(updatedFriend => {
		res.status(200).send(updatedFriend)
	})
	.catch(error => res.status(500).send(error))
}

module.exports.unfriend = async (req,res) => {
	await User.findById(req.user.id)
	.then(foundUser => {
		if(foundUser){
			let foundFriend = foundUser.friends.find(user=>{return user.userId===req.params.id})
			let friendIndex = foundUser.friends.indexOf(foundFriend)
			if(foundFriend){
				let removedFriend = foundUser.friends.splice(friendIndex,1)
				User.findById(removedFriend[0].userId)
				.then(foundUser => {
					let user = foundUser.friends.find(user=>{
						return user.userId===req.user.id
					})
					let userIndex = foundUser.friends.indexOf(user)
					let removedUser = foundUser.friends.splice(userIndex,1)
					foundUser.save()
				})
				return foundUser.save()
			} else {
				res.status(404).send({message: "User not on friends list."})
			}
		} else {
			res.status(404).send({message: "User not found."})
		}
	})
	.then(updatedUser=> {
		res.status(200).send(updatedUser)
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getUser = async (req,res) => {
	await User.findById(req.params.id,{_id:0,isAdmin:0,password: 0,createdAt:0,updatedAt:0,__v:0})
	.then(foundUser => {
		if(foundUser){
			if(foundUser.isActive===false){
				res.status(400).send({message: "User is inactive."})
			} else {
				res.status(200).send(foundUser)
			}
		} else {
			res.status(404).send({message: "User does not exist."})
		}
	})
	.catch(error => res.status(500).send(error))
}


module.exports.searchByQuery = async (req,res) => {
	await User.find({
		//--FULL TEXT SEARCH--//
		// $text: { $search: req.body.query }

		//--PARTIAL TEXT SEARCH (regex)--//
		$or:[
			{username: {$regex: new RegExp(req.body.query.toLowerCase())}},
			{firstName: {$regex: new RegExp(req.body.query.toLowerCase())}},
			{lastName: {$regex: new RegExp(req.body.query.toLowerCase())}},
			{email: {$regex: new RegExp(req.body.query.toLowerCase())}}
		]
	},{_id:0,isAdmin:0,password: 0,createdAt:0,updatedAt:0,__v:0})
	.then(foundUser => {
		if(foundUser.length>0){
			res.status(200).send(foundUser)
		} else {
			res.status(404).send({message: "No matches found."})
		}
	})
	.catch(error => res.status(500).send(error))
}