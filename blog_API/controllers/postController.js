const User = require('./../models/User')
const Post = require('./../models/Post')

module.exports.create = async (req,res) => {
	let newPost = new Post({
		title: req.body.title.toLowerCase(),
		description: req.body.description.toLowerCase(),
		photo: req.body.photo,
		userId: req.user.id,
		categories: req.body.categories.toLowerCase()
	})
	await newPost.save()
	.then(post=>{
		res.status(200).send({post, mesage: "Post Created!"})
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getByUser = async (req,res) => {
	await Post.find({userId: req.user.id})
	.then(foundPosts=>{
		if(foundPosts.length>0){
			res.status(200).send(foundPosts)
		} else {
			res.status(404).send({message: "User has no post yet."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getById = async (req,res) => {
	await Post.findById(req.params.id)
	.then(foundPost => {
		if(foundPost){
			res.status(200).send(foundPost)
		} else {
			res.status(404).send({message: "Post does not exist"})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.update = async (req,res) => {
	let post = await Post.findById(req.params.id)
	let updates = {
		photo: req.body.photo===undefined?post.photo:req.body.photo,
		title: req.body.title===undefined?post.title:req.body.title.toLowerCase(),
		description: req.body.description===undefined?post.description:req.body.description.toLowerCase(),
		categories: req.body.categories===undefined?post.categories:req.body.categories
	}
	await Post.findById(req.params.id)
	.then(post => {
		if(post){
			if(post.userId===req.user.id){
				Post.find({$and:
					[{userId: req.user.id},{title:updates.title}]
				})
				.then(foundPosts=>{
					if(foundPosts.length>0){
						res.status(400).send({message: "Same user has another post with the same title."})
					} else {
						Post.findByIdAndUpdate(req.params.id,updates,{new:true})
						.then(post => {
							res.status(200).send({post, message: "Post has been updated."})
						})
					}
				})

			} else {
				res.status(401).send({message: "You are not authorized to edit this post."})
			}
		} else {
			res.status(404).send({message: "Post does not exist."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.delete = async (req,res) =>{
	await Post.findById(req.params.id)
	.then(post=>{
		if(post){
			if(post.userId===req.user.id){
				Post.findByIdAndDelete(req.params.id)
				.then((deletedItem) => {
					console.log(deletedItem)
					res.status(200).send({message:"Post has been deleted."})
				})
				.catch(error => res.send(error))
			} else {
				res.status(401).send({message: "You are not authorized to delete this post."})
			}
		} else {
			res.status(404).send({message: "Nothing to delete."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getAll = async (req,res) =>{
	await Post.find()
	.then(allPosts=>{
		if(allPosts.length>0){
			res.status(200).send(allPosts)
		} else {
			res.status(404).send({message: "No post has been made yet by any user."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.searchByQuery = async (req,res) => {
	await Post.find({
		//--FULL TEXT SEARCH--//
		// $text: { $search: req.body.query }

		//--PARTIAL TEXT SEARCH (regex)--//
		$or:[
			{title: {$regex: new RegExp(req.body.query)}},
			{description: {$regex: new RegExp(req.body.query)}}
		]
	},{_id:0, __v:0,})
	.then(foundPosts => {
		if(foundPosts.length>0){
			res.status(200).send(foundPosts)
		} else {
			res.status(404).send({message: "No matches found."})
		}
	})
	.catch(error => res.status(500).send(error))
}