const Category = require('./../models/Category')

module.exports.create = async (req,res) => {
	let newCategory = new Category({
		name: req.body.name.toLowerCase(),
		userId: req.user.id,
	})
	await newCategory.save()
	.then(category=>{
		res.status(200).send({category, mesage: "New category reated!"})
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getByUser = async (req,res) => {
	await Category.find({userId: req.user.id})
	.then(foundCategory=>{
		if(foundCategory.length>0){
			res.status(200).send(foundCategory)
		} else {
			res.status(404).send({message: "User has not made any category yet."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getAll = async (req,res) =>{
	await Category.find()
	.then(allCategories=>{
		if(allCategories.length>0){
			res.status(200).send(allCategories)
		} else {
			res.status(404).send({message: "No category has been made by any user yet."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.getById = async (req,res) => {
	await Category.findById(req.params.id)
	.then(foundCategory => {
		if(foundCategory){
			res.status(200).send(foundCategory)
		} else {
			res.status(404).send({message: "Category does not exist"})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.update = async (req,res) => {
	let category = await Category.findById(req.params.id)
	let updates = {
		name: req.body.name===undefined?category.name:req.body.name.toLowerCase()
	}
	await Category.findById(req.params.id)
	.then(category => {
		if(category){
			if(category.userId===req.user.id){
				Category.find({name: updates.name})
				.then(foundCategory=>{
					if(foundCategory.length>0){
						res.status(400).send({message: "Category already exists."})
					} else {
						Category.findByIdAndUpdate(req.params.id,updates,{new:true})
						.then(category => {
							res.status(200).send({category, message: "category has been updated."})
						})
					}
				})
			} else {
				res.status(401).send({message: "You are not authorized to edit this category."})
			}
		} else {
			res.status(404).send({message: "Category does not exist."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.delete = async (req,res) =>{
	await Category.findById(req.params.id)
	.then(category=>{
		if(category){
			if(category.userId===req.user.id){
				Category.findByIdAndDelete(req.params.id)
				.then((deletedItem) => {
					console.log(deletedItem)
					res.status(200).send({message:"Category has been deleted."})
				})
			} else {
				res.status(401).send({message: "You are not authorized to delete this category."})
			}
		} else {
			res.status(404).send({message: "Nothing to delete."})
		}
	})
	.catch(error=>res.status(500).send(error))
}

module.exports.searchByQuery = async (req,res) => {
	await Category.find({
		//--FULL TEXT SEARCH--//
		// $text: { $search: req.body.query }

		//--PARTIAL TEXT SEARCH (regex)--//
		name: {$regex: new RegExp(req.body.query)}
	},{_id:0, __v:0,})
	.then(foundCategory => {
		if(foundCategory.length>0){
			res.status(200).send(foundCategory)
		} else {
			res.status(404).send({message: "No matches found."})
		}
	})
	.catch(error => res.status(500).send(error))
}