const router = require('express').Router()
const validate = require('../utils/validation')
const {verifyUser} = require('../utils/auth')
const category = require('../controllers/categoryController')


router.post('/', verifyUser, validate.isActiveUser, validate.categoryExists, category.create)

router.get('/',verifyUser, validate.isActiveUser, category.getByUser)

router.get('/all',verifyUser, validate.isActiveUser,category.getAll)

router.get('/search', category.searchByQuery)

router.get('/:id',verifyUser, validate.isActiveUser, category.getById)

router.put('/:id',verifyUser, validate.isActiveUser, category.update)

router.delete('/:id',verifyUser, validate.isActiveUser, category.delete)


module.exports = router