const router = require('express').Router()
const validate = require('../utils/validation')
const {verifyUser} = require('../utils/auth')
const post = require('../controllers/postController')


router.post('/', verifyUser, validate.isActiveUser, validate.postExists, post.create)

router.get('/',verifyUser,post.getByUser)

router.get('/all',post.getAll)

router.get('/search', post.searchByQuery)

router.get('/:id',verifyUser,post.getById)

router.put('/:id',verifyUser,post.update)

router.delete('/:id',verifyUser,post.delete)

module.exports = router