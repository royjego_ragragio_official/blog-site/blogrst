const router = require('express').Router()
const validate = require('../utils/validation')
const auth = require('../utils/auth')
const user = require('../controllers/userController')


router.post('/',validate.userExists, user.register)

router.post('/login', user.login)

router.get('/', auth.verifyUser, user.getDetails)

router.get('/active', user.getActiveUsers)

router.get('/search', user.searchByQuery)

router.get('/:id', user.getUser)

router.put('/', auth.verifyUser, validate.isActive, user.updateDetails)

router.put('/deactivate', auth.verifyUser, validate.isActive,  user.deactivate)

router.put('/reactivate', auth.verifyUser, validate.isActive,  user.reactivate)
	
router.put('/changepassword', auth.verifyUser, validate.isActive, validate.checkPassword, user.changePassword)

router.put('/unfriend/:id',auth.verifyUser, validate.isActive,  user.unfriend)

router.put('/:id',auth.verifyUser, validate.isActive, validate.isFriend, user.addFriend)





module.exports = router