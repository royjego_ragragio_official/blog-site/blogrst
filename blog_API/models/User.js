const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
	username: {
        type: String,
        required: [true, "Username required"],
        unique: true
    },
    firstName: {
        type: String,
        required: [ true, "First name required"]

    },
    lastName: {
        type: String,
        required: [ true, "Last name required"]
    },
    email: {
        type: String,
        default: "",
        unique: true
    },
    password: {
        type: String,
        required: [true, "Password required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isActive: {
        type: Boolean,
        default: true
    },
    profilePic: {
    	type: String,
    	default: ""
    },
    mobileNo: {
        type: String,
        default: ""
    },
    gender: {
        type: String,
        default: ""
    },
    birthday: {
        month: {
            type: String
        },
        date: {
            type: Number
        },
        year: {
            type: Number
        }
    },
    currentAddress: {
        line1: {
            type: String,
            default: ""
        },
        street: {
            type: String,
            default: ""
        },
        subdivision: {
            type: String,
            default: ""
        },
        district: {
            type: String,
            default: ""
        },
        city: {
            type: String,
            default: ""
        },
        province: {
            type: String,
            default: ""
        },
        region: {
            type: String,
            default: ""
        },
        country: {
            type: String,
            default: ""
        },
        postalCode: {
            type: String,
            default: ""
        }
    },
    permanentAddress: {
        line1: {
            type: String,
            default: ""
        },
        street: {
            type: String,
            default: ""
        },
        subdivision: {
            type: String,
            default: ""
        },
        district: {
            type: String,
            default: ""
        },
        city: {
            type: String,
            default: ""
        },
        province: {
            type: String,
            default: ""
        },
        region: {
            type: String,
            default: ""
        },
        country: {
            type: String,
            default: ""
        },
        postalCode: {
            type: String,
            default: ""
        }
    },
    friends: [
        {
            userId: {
                type: String,
                required: [true, "Friend Id is required."]
            },
            addedOn: {
                type: Date,
                default: new Date()
            },
            username:{ //remove this later only for dev
                type: String,
                required: false
            }
        }
    ]
    
},{timestamps: true})

UserSchema.index({
    username:'text',
    firstName:'text',
    lastName:'text',
    email:'text'
})

module.exports = mongoose.model('User', UserSchema)