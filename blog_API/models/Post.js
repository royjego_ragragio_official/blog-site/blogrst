const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
	title: {
        type: String,
        required: [true, "Post title required."]
    },
    description: {
        type: String,
        required: [true, "Post description is required."]
    },
    photo: {
        type: String,
        default: ""
    },
    userId: {
        type: String,
        required: [true, "User Id required"]
    },
    categories: {
        type: Array,
        required: false
    }
},{timestamps: true})

PostSchema.index({title:'text',description:'text'})

module.exports = mongoose.model('Post', PostSchema)