const User = require('../models/User')
const Post = require('../models/Post')
const Category = require('../models/Category')
const bcrypt = require('bcrypt')

module.exports.userExists = (req,res,next) => {
	User.findOne({
		$or:[{username: req.body.username.toLowerCase()},{email: req.body.email.toLowerCase()},
		{$and:[{firstName: req.body.firstName.toLowerCase()},{lastName: req.body.lastName.toLowerCase()}]}]
	})
	.then(foundResult => {
		if(foundResult){
			if(foundResult.username===req.body.username.toLowerCase()){
				res.status(400).send({error: "Username already taken."})
			} else if(foundResult.email===req.body.email.toLowerCase()){
				res.status(400).send({error: "Email already registered."})
			} else if(foundResult.firstName===req.body.firstName.toLowerCase()&&foundResult.lastName===req.body.lastName.toLowerCase()){
				res.status(400).send({error: "User with the same name is already registered."})
			}
		} else {
			next()
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.postExists = async (req,res,next) => {
	await Post.find({userId: req.user.id})
	.then(foundResult => {
		let foundPost = foundResult.find(post => {
			return post.title.toLowerCase()===req.body.title.toLowerCase()
		})
		if(foundPost){
			res.status(400).send({error: "Post with the same title already exists."})
		} else {
			next()
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.isActiveUser = async (req,res,next) => {
	await User.findById(req.user.id)
	.then(foundUser => {
		if(foundUser.isActive===false){
			res.status(400).send({message: "Can not post! Your account is deactivated. Please reactivate account before doing something."})
		} else {
			next()
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.categoryExists = async (req,res,next) => {
	await Category.find({name: req.body.name.toLowerCase()})
	.then(foundResult => {
		if(foundResult.length>0){
			res.status(400).send({message: "Category already exists."})
		} else {
			next()
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.checkPassword = async (req,res,next) => {
	await User.findById(req.user.id)
	.then(foundUser => {
		if(foundUser){
			if(foundUser._id.toString()===req.user.id){
				const isPasswordCorrect = bcrypt.compareSync(req.body.currentPassword, foundUser.password)
				if(isPasswordCorrect){
					if(req.body.newPassword===req.body.confirmNewPassword){
						next()
					} else {
						res.status(400).send({error: "New passwords does not match."})
					}
				} else {
					res.status(400).send({error: "Invalid credentials."})
				}
			} else {
				res.status(401).send({message: "You are not authorized to change the password of this user."})
			}
		} else {
			res.status(404).send({message: "User does not exist."})
		}
	})
	.catch(error => res.status(500).send(error))
}

module.exports.isFriend = async (req,res,next) => {
	if(req.user.id===req.params.id){
		return res.status(400).send({message: "You can not add yourself."})
	} else {
		await User.findById(req.user.id)
		.then(foundUser => {
			if(foundUser.friends.length===0){
				next()
			} else {
				let foundFriend = foundUser.friends.find(user=>{
					return user.userId===req.params.id
				})
				if(foundFriend){
					res.status(400).send({message: "User you're trying to add is already your friend."})
				} else {
					next()
				}
			}
		})
		.catch(error => res.status(500).send(error))
	}
}

module.exports.isActive = async (req,res,next) => {
	await User.findById(req.user.id)
	.then(foundUser => {
		if(foundUser){
			if(foundUser.isActive===false){
				res.status(400).send({message: "Please reactivate your account before doing anything."})
			} else {
				next()
			}
		} else {
			res.status(404).send({message: "User not found."})
		}
	})
	.catch(error => res.status(500).send())
}