const jwt = require("jsonwebtoken")
const secret = process.env.secret

module.exports.createAccessToken = (user) => {

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
        isActive: user.isActive,
        username: user.username
    }
    return jwt.sign(data,secret,{})

}

module.exports.verifyUser = (req,res,next) => {

    let token = req.headers.authorization
    if(typeof token === "undefined"){
        res.status(401).send({auth: "You are not a registered user."})
    } else {
        token = token.slice(7,token.length)
        jwt.verify(token,secret, function(err,decoded){
            if(err){
                res.status(401).send({auth:"You are not a registered user."})
            } else {
                req.user = decoded
                next()
            }
        })
    }
}

module.exports.verfiyAdmin = (req,res,next) => {
    if(req.user.isAdmin){
        next()
    } else {
        res.status(401).send({auth: "You are not an admin."})
    }
}


