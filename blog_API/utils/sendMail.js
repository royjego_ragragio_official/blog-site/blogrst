const User = require('./../models/User')
const nodemailer = require('nodemailer')
const {google} = require('googleapis')
const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRET = process.env.CLIENT_SECRET
const REDIRECT_URI = process.env.REDIRECT_URI
const REFRESH_TOKEN = process.env.REFRESH_TOKEN
const EMAIL_OFFICIAL = process.env.EMAIL_OFFICIAL
const EMAIL = process.env.EMAIL
const {capitalizeFirstChar} = require('./tools')

const oAuth2Client = new google.auth.OAuth2(CLIENT_ID,CLIENT_SECRET,REDIRECT_URI)
oAuth2Client.setCredentials({refresh_token: REFRESH_TOKEN})

module.exports.sendRegisterMail = async (user) =>{
    let name = capitalizeFirstChar(`${user.firstName} ${user.lastName}`)
    const mailAccessToken = await oAuth2Client.getAccessToken()
    const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            type: 'OAuth2',
            user: EMAIL_OFFICIAL,
            clientId: CLIENT_ID,
            clientSecret: CLIENT_SECRET,
            refreshToken: REFRESH_TOKEN,
            accessToken: mailAccessToken
        }
    })
    const mailOptions = {
        from: 'My Blog App <EMAIL_OFFICIAL>',
        to: user.email.toLowerCase(),
        subject: 'Registration Successful',
        text: `
        Hello ${name}!

        Your username is ${user.username.toLowerCase()}.
        The email you used is ${user.email.toLowerCase()}.

        Thank you for registering at My Blog App!
        Have fun and show the world your blogs.

        Sincerely,
        Roy Jego Ragragio`
    }
    await transport.sendMail(mailOptions)
    .then(data => console.log(data))
    .catch(error => console.loh(error))
}

module.exports.sendRegistryMail = async (user) =>{
    let name = capitalizeFirstChar(`${user.firstName} ${user.lastName}`)
    const mailAccessToken = await oAuth2Client.getAccessToken()
    const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            type: 'OAuth2',
            user: EMAIL_OFFICIAL,
            clientId: CLIENT_ID,
            clientSecret: CLIENT_SECRET,
            refreshToken: REFRESH_TOKEN,
            accessToken: mailAccessToken
        }
    })
    const mailOptions = {
        from: 'My Blog App <EMAIL_OFFICIAL>',
        to: EMAIL,
        subject: 'New user from My Blog App',
        text: `
        A new user has just signed up!

        Username: ${user.username.toLowerCase()}
        Name: ${name}
        Email: ${user.email.toLowerCase()}

        Thank you note sent!
        `
    }
    await transport.sendMail(mailOptions)
    .then(data => console.log(data))
    .catch(error => console.loh(error))
}

module.exports.sendChangePasswordMail = async (user) =>{
    let name = capitalizeFirstChar(`${user.firstName} ${user.lastName}`)
    const mailAccessToken = await oAuth2Client.getAccessToken()
    .catch(error=>console.log(error))
    const transport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            type: 'OAuth2',
            user: EMAIL_OFFICIAL,
            clientId: CLIENT_ID,
            clientSecret: CLIENT_SECRET,
            refreshToken: REFRESH_TOKEN,
            accessToken: mailAccessToken
        }
    })
    const mailOptions = {
        from: 'My Blog App <EMAIL_OFFICIAL>',
        to: user.email.toLowerCase(),
        subject: 'Password has been changed!',
        text: `
        Your password has been changed!

        Updated: ${user.updatedAt}

        ACCOUNT:
        Username: ${user.username.toLowerCase()}
        Name: ${name}
        Email: ${user.email.toLowerCase()}


        If it is you who changed your password, please disregard this mail.

        If NOT, please contact our customer support:
        email: ${EMAIL_OFFICIAL}

        We apologize for this inconvenience.

        Sincerely,
        Roy Jego Ragragio
        `
    }
    await transport.sendMail(mailOptions)
    .then(data => console.log(data))
    .catch(error => console.loh(error))
}