module.exports.capitalizeFirstChar = (fullName) =>{
    fullName = fullName.toLowerCase().split(' ')
    for(i in fullName){
        fullName[i]=fullName[i].charAt(0).toUpperCase() + (fullName[i].slice(1));
    }
    return fullName.join(' ')
}