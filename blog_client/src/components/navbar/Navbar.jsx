import React from 'react'
import './Navbar.css'

export default function Navbar() {
	return (
		<div className="Navbar">
			<div className="logo">Blogr</div>
			<div className="nav-menu">
				<i className="fas fa-home"></i>
				<i className="fas fa-user"></i>
				<i className="fas fa-bell"></i>
			</div>
			<div className="dropdown">
				<span className="prof-pic">M</span>
				<span className="prof-name">Malia Skye</span>
				<i className="fas fa-caret-down"></i>
			</div>
		</div>
	)
};