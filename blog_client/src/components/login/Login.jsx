import React from 'react'
import './Login.css'

export default function Login({ setIsLoggedIn }) {
	return (
		<div className="Login">
			<button onClick={()=>setIsLoggedIn(true)}> Test Login</button>
		</div>
	)
};