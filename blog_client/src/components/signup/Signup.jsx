import React, { useState,useEffect } from 'react'
import './Signup.css'
import { currentDate,
		toggleShowPassword,
		validateEmail,
		validatePassword,
		prettyDate } from '../../utils/utilFunctions.js'

export default function Signup({ setWithAccount }) {
	const [firstName,setFirstName] = useState('')
	const [lastName,setLastName] = useState('')
	const [email,setEmail] = useState('')
	const [password,setPassword] = useState('')
	const [confirmPassword,setConfirmPassword] = useState('')
	const [birthday,setBirthday] = useState('')
	const [gender,setGender] = useState('')
	const [showPassword,setShowPassword] = useState(false)
	const [focused,setFocused] = useState('')
	const [isClicked,setIsClicked] = useState(false)
	const [validFirstName,setValidFirstName] = useState(null)
	const [validLastName,setValidLastName] = useState(null)
	const [validEmail,setValidEmail] = useState(null)
	const [validPassword,setValidPassword] = useState(null)
	const [validConfirmPass,setValidConfirmPass] = useState(null)
	const [validCreate,setValidCreate] = useState(false)

	
	function inputChecker(input){
		if(input==="firstName"){
			firstName.length>=3?setValidFirstName(true):(firstName.length===0?setValidFirstName(null):setValidFirstName(false))
		}

		if(input==="lastName"){
			lastName.length>=3?setValidLastName(true):(lastName.length===0?setValidLastName(null):setValidLastName(false))
		}

		if(input==="email"){
			email.length!==0&&validateEmail.test(email.toString())?setValidEmail(true):(email.length===0?setValidEmail(null):setValidEmail(false))
		}

		if(input==="password"){
			password.match(validatePassword)?setValidPassword(true):(password.length===0?setValidPassword(null):setValidPassword(false));
			confirmPassword.length===0?setValidConfirmPass(null):(validPassword&&confirmPassword===password?setValidConfirmPass(true):setValidConfirmPass(false));
		}

		if(input==="confirmPassword"){
			confirmPassword.length===0?setValidConfirmPass(null):(validPassword&&confirmPassword===password?setValidConfirmPass(true):setValidConfirmPass(false))
		}
	}



	useEffect(()=>{
		let signupBtn = document.querySelector('.signup-form-btn')

		if(focused&&!isClicked){
			inputChecker(focused)
		} else if(focused&&isClicked){
			setFocused('')
			setIsClicked(false)
		} else {
			return null
		}

		if(validFirstName&&validLastName&&validEmail&&validPassword&&validConfirmPass&&birthday&&gender){
			signupBtn.disabled = false
			setValidCreate(true)
		} else {
			signupBtn.disabled = true
			setValidCreate(false)
		}

	},[firstName,lastName,email,password,confirmPassword,birthday,gender,focused,validFirstName,validLastName,validEmail,validPassword,validConfirmPass,validCreate,isClicked])


	return (
		<div className="signup">
			<form>
				<div className="form-div name">
				    <div>
					    <input type="text"
					    	placeholder=" "
					    	className={`form-input ${validFirstName?'success':`${validFirstName===false?'danger':null}`}`}
					    	value={firstName}
					    	id="firstName"
					    	onKeyUp={(e)=>setFocused(e.target.id)}
					    	onClick={(e)=>setFocused(e.target.id)}
					    	onChange={(e)=>{
					    		setFirstName(e.target.value)
					    	}}
					    />
					    <label htmlFor="firstName">First Name</label>
					</div>
					<div>
					    <input type="text"
						    placeholder=" "
						    className={`form-input ${validLastName?'success':`${validLastName===false?'danger':null}`}`}
						    value={lastName}
					    	id="lastName"
					    	onKeyUp={(e)=>setFocused(e.target.id)}
					    	onClick={(e)=>setFocused(e.target.id)}
					    	onChange={(e)=>{
					    		setLastName(e.target.value)
					    	}}
					/>
					    <label htmlFor="lastName">Last Name</label>
					</div>
				</div>
				<div className="form-div email">
					<input type="text"
						placeholder=" "
						className={`form-input ${validEmail?'success':`${validEmail===false?'danger':null}`}`}
						value={email}
					    id="email"
						onKeyUp={(e)=>setFocused(e.target.id)}
						onClick={(e)=>setFocused(e.target.id)}
				    	onChange={(e)=>{
				    		setEmail(e.target.value)
				    	}}
					/>
					<label htmlFor="email">Email</label>
				</div>
				<div className="form-div password">
					<input type="password"
						placeholder=" "
						className={`form-input ${validPassword?'success':`${validPassword===false?'danger':null}`}`}
						value={password}
						id="password"
						onKeyUp={(e)=>setFocused(e.target.id)}
						onClick={(e)=>setFocused(e.target.id)}
				    	onChange={(e)=>{
				    		setPassword(e.target.value)
				    	}}
					/>
					<label htmlFor="password">Password</label>
					{
						showPassword
						?
						<i className="fas fa-eye" onClick={()=>{
								toggleShowPassword()
								setShowPassword(!showPassword)
							}}
						>
							
						</i>
						:
						<i className="fas fa-eye-slash" onClick={()=>{
								toggleShowPassword()
								setShowPassword(!showPassword)
							}}
						>
							
						</i>
					}
				</div>
				<div className="form-div confirm">
					<input type="password" 
						placeholder=" " 
						className={`form-input ${validConfirmPass?'success':`${validConfirmPass===false?'danger':null}`}`}
						value={confirmPassword}
						id="confirmPassword"
						onKeyUp={(e)=>setFocused(e.target.id)}
						onClick={(e)=>setFocused(e.target.id)}
				    	onChange={(e)=>{
				    		setConfirmPassword(e.target.value)
				    	}}
					/>
					<label htmlFor="confirmPassword">Confirm Password</label>
					{
						showPassword
						?
						<i className="fas fa-eye" onClick={()=>{
								toggleShowPassword()
								setShowPassword(!showPassword)
							}}
						>
							
						</i>
						:
						<i className="fas fa-eye-slash" onClick={()=>{
								toggleShowPassword()
								setShowPassword(!showPassword)
							}}
						>
							
						</i>
					}
				</div>
				<div className="form-div select">
					<div>
						<input type="date" 
							id="birthday"
							className={birthday?'success':null}
							min={`${currentDate('year')-100}-${currentDate('month')}-${currentDate('date')}`}
							max={currentDate()}
							onChange={(e)=>{setBirthday(prettyDate(e.target.value))}}
							onClick={(e)=>setFocused(e.target.id)}
						/>
					</div>
					<div className={`gender ${gender?'success':null}`}>
						<label htmlFor="male">Male</label>
						<br/>
					    <input type="radio" 
				    		id="male" 
				    		name="gender" 
				    		value="male"
				    		onClick={(e)=>{
				    			setGender(e.target.id)
				    			setFocused(e.target.id)
				    		}}
				    		// onChange={(e)=>setGender(e.target.id)}
				    		// checked={gender}
				    		
					    />
						<label htmlFor="female">Female</label>
						<br/>		    
						<input type="radio" 
							id="female" 
							name="gender" 
							value="female"
				    		onClick={(e)=>{
				    			setGender(e.target.id)
				    			setFocused(e.target.id)
				    		}}
							// onChange={(e)=>setGender(e.target.id)}
							// checked={gender}
						/>
					</div>
				</div>
				<div className="form-div switch">
					<p onClick={()=>setWithAccount(true)}>Already have an account?</p>
				</div>
				<div className="form-div button">
					<button className={`signup-form-btn ${validCreate?'validCreate':null}`}
						type="button"
						onClick={(e)=>{
							e.preventDefault()
							console.log(firstName)
							console.log(lastName)
							console.log(email)
							console.log(password)
							console.log(confirmPassword)
							console.log(birthday)
							console.log(gender)
						}}
					>
						create account
					</button>
				</div>
			</form> 
		</div>
	)
};