import React from 'react'
import './Footer.css'
import { Link } from 'react-router-dom'
import { clearTimeOuts } from '../../utils/utilFunctions.js'

export default function Footer() {
	return (
		<div className="footer">
		    <div className="footer-top">
		    	<div>
		    		<h4>About Us</h4>
		    		<ul>
		    			<Link to="/history" onClick={()=>clearTimeOuts()}>History</Link>
		    			<Link to="/missionvision">Mission & Vision</Link>
		    			<Link to="/founder">Founder</Link>
		    		</ul>
		    	</div>
		    	<div>
		    		<h4>Go to</h4>
		    		<ul>
		    			<Link to="/" onClick={()=>clearTimeOuts()}>Register</Link>
		    			<Link to="/">Login</Link>
		    		</ul>
		    	</div>

		    	<div>
		    		<h4>Contact Us</h4>
		    		<ul>
		    			<li>
		    				<span><i className="fas fa-mobile-alt"></i></span>
		    				<span>+639121234567</span>
		    			</li>
		    			<li>
		    				<span><i className="far fa-envelope"></i></span>
		    				<span>helpdesk@blogr.com</span>
		    			</li>
		    		</ul>
		    	</div>
		    	<div>
		    		<h4>Follow Us</h4>
		    		<ul>
		    			<li>
		    				<span><i className="fab fa-facebook-f"></i></span>
		    				<span>/blogr</span>
		    			</li>
		    			<li>
		    				<span><i className="fab fa-twitter"></i></span>
		    				<span>/blogr</span>
		    			</li>
		    			<li>
		    				<span><i className="fab fa-instagram"></i></span>
		    				<span>/blogr</span>
		    			</li>
		    		</ul>
		    	</div>
		    </div>
			<div className="footer-bottom">
				<span>&copy; 2021 <span>Blogr</span>  |  All rights reserved.</span>
				<span>Made with <i className="fas fa-heart"></i> and <i className="fas fa-coffee"></i></span>
			</div>
		</div>
	)
};