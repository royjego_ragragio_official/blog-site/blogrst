import './App.css';
import { useState } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Routes from './Routes.jsx'
// import Navbar from './components/navbar/Navbar.jsx'
// import Landing from './pages/landing/landing.jsx'
// import Blogfeed from './pages/blogfeed/blogfeed.jsx'

function App() {
    
    const [isLoggedIn,setIsLoggedIn] = useState(false)
    const [resetType,setResetType] = useState(false)
    console.log(resetType)

    return (
        // isLoggedIn
        // ?
        // <>
        //     <Navbar />
        //     <div id="main-container">
        //         <Router>
        //             <Routes
        //                 isLoggedIn={isLoggedIn}
        //                 setIsLoggedIn={setIsLoggedIn}
        //             />
        //         </Router>
        //     </div>
        // </>
        // :
        // <Landing />

        <Router>
            <Routes
                isLoggedIn={isLoggedIn}
                setIsLoggedIn={setIsLoggedIn}
                resetType={resetType}
                setResetType={setResetType}
            />
        </Router>

    );
}

export default App;
