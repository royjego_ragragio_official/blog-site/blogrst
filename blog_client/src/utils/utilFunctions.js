export const currentDate = (query) => {
	let today = new Date();
	let dd = String(today.getDate()).padStart(2, '0');
	let mm = String(today.getMonth() + 1).padStart(2, '0');
	let yyyy = today.getFullYear(); 
	
	if(query==='month'){
		return mm
	} else if(query==='date'){
		return dd
	} else if(query==='year'){
		return yyyy
	}else {
		return `${yyyy}-${mm}-${dd}`
	}
}


export const capitalizeName = (name) => {
	name = name.toLowerCase().split(' ')
	for(let i=0;i<name.length;i++){
		name[i] = name[i].charAt(0).toUpperCase() + (name[i].slice(1));
	}
	return name.join(' ')
}

export const validateEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const validatePassword = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-/[\]`~_()={}|:;"'<>\\ ]).{8,}$/

export const toggleShowPassword = () => {
	let passField = document.querySelector('#password')
	let confirmField = document.querySelector('#confirmPassword')
	passField.type==='password'?passField.type='text':passField.type='password'
	confirmField.type==='password'?confirmField.type='text':confirmField.type='password'
}

export const prettyMonth = (month) => {
	if(month===1){
		return 'January'
	} else if(month===2){
		return 'February'
	} else if(month===3){
		return 'March'
	} else if(month===4){
		return 'April'
	} else if(month===5){
		return 'May'
	} else if(month===6){
		return 'June'
	} else if(month===7){
		return 'July'
	} else if(month===8){
		return 'August'
	} else if(month===9){
		return 'September'
	} else if(month===10){
		return 'October'
	} else if(month===11){
		return 'November'
	} else if(month===12){
		return 'December'
	}
}

export const prettyDate = (date) => {
	let year = date.slice(0,4)
	let month = parseInt(date.slice(5,7))
	let day = date.slice(8,10)

	return `${prettyMonth(month)} ${day}, ${year}`
}


	let i = 0;
	let speed = 40
	let tagTimeout;
export const typeTag = () => {
	let tag = 'Spot. Stop. Snap. Show';
	let text = 'Be a BLogr. Connect with people and show your daily blogs to the world.'
	if (i < tag.length) {
		document.querySelector('.tag').innerHTML += tag.charAt(i);
		i++;
		tagTimeout = setTimeout(typeTag, speed);

		if(i === tag.length){
			// clearTimeOuts(tagTimeout)
			// i=0
		}
	}

}

	let x = 0;
	let textTimeout;
export const typeText = () => {
	let tag = 'Spot. Stop. Snap. Show';
	let text = 'Be a BLogr. Connect with people and show your daily blogs to the world.'
	if (x < text.length) {
		document.querySelector('.text').innerHTML += text.charAt(x);
		x++;
		textTimeout = setTimeout(typeText, speed-25);

		if(x === text.length){
			// clearTimeOuts(textTimeout)
			// x=0
		}
	}
}

export const clearTimeOuts = () => {
	// console.log(timeout)
	console.log('test')
}