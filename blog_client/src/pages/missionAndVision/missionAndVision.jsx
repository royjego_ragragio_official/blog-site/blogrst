import React from 'react'
import './missionAndVision.css'
import Footer from '../../components/footer/Footer.jsx'

export default function MissionAndVision() {
	return (
		<>
			<div className="missionAndVision">
				mission And Vision
			</div>
			<div className="footer-wrapper">
				<Footer />
			</div>
		</>
	)
};