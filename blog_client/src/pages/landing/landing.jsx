import React, { useState,useRef,useEffect } from 'react'
import './landing.css'
import Signup from '../../components/signup/Signup.jsx'
import Login from '../../components/login/Login.jsx'
import Footer from '../../components/footer/Footer.jsx'
import { typeTag,typeText } from '../../utils/utilFunctions.js'

export default function Landing({ setIsLoggedIn,resetType }) {

	const [withAccount, setWithAccount] = useState(false)

	useEffect(()=>{
		typeTag()
		setTimeout(()=>{typeText()},1000)
		// console.log('landing render')
	},[resetType])

	return (
		<>
			<div className="landing">
				<div className="landing-inner">
					<div className="landing-left">
						<h1>Blogr</h1>
						{/*<span><p ref={tagRef}></p></span>*/}
						<p className="tag"></p>

						<p className="text"></p>
					</div>
					<div className="landing-right">
						{
							withAccount
							?
							<Login
								setWithAccount={setWithAccount}
								setIsLoggedIn={setIsLoggedIn}
							/>
							:
							<Signup setWithAccount={setWithAccount}/>
						}
					</div>
				</div>
			</div>
			<div className="footer-wrapper">
				<Footer />		
			</div>
		</>
	)
};