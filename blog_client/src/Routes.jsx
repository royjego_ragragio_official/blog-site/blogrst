import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Blogfeed from './pages/blogfeed/blogfeed.jsx'
import Landing from './pages/landing/landing.jsx'
import History from './pages/history/history.jsx'
import MissionAndVision from './pages/missionAndVision/missionAndVision.jsx'
import Founder from './pages/founder/founder.jsx'

export default function Routes({ isLoggedIn,setIsLoggedIn,resetType,setResetType }) {
	// console.log(withAccount)
	return (
		<Switch>
			{
				isLoggedIn
				?
				<Route exact path='/'
	                render={(props)=>(
	                    <Blogfeed {...props}
	                        setIsLoggedIn={setIsLoggedIn}
	                        resetType={resetType}
	                        setResetType={setResetType}
	                    />
	                )}
	            />
				:
				<Route exact path='/'
	                render={(props)=>(
	                    <Landing {...props}
	                        setIsLoggedIn={setIsLoggedIn}
	                        resetType={resetType}
	                    />
	                )}
	            />
			}

			<Route exact path='/history'
                render={(props)=>(
                    <History {...props}
                        setIsLoggedIn={setIsLoggedIn}
                        resetType={resetType}
                    />
                )}
            />

            <Route exact path='/missionvision'
                render={(props)=>(
                    <MissionAndVision {...props}
                        setIsLoggedIn={setIsLoggedIn}
                        resetType={resetType}
                    />
                )}
            />

            <Route exact path='/founder'
                render={(props)=>(
                    <Founder {...props}
                        setIsLoggedIn={setIsLoggedIn}
                        resetType={resetType}
                    />
                )}
            />
			
			
		</Switch>
	)
};